package com.haidev.root.demomaterialdrawer

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var result: Drawer? = null
    private lateinit var headerResult: AccountHeader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DrawerBuilder().withActivity(this).build()

        headerResult = AccountHeaderBuilder()
                .withActivity(this)
                .withSelectionListEnabled(false)
                .withTranslucentStatusBar(true)
                .withHeaderBackground(R.color.colorPrimary)
                .withSavedInstance(savedInstanceState)
                .build()

        val profile = ProfileDrawerItem().withName("Demo Drawer").withEmail("cobadrawer@gmail.com").withIdentifier(100).withIcon(R.drawable.ic_launcher_background)
        headerResult.activeProfile = profile

        val drawerBuilder: DrawerBuilder = DrawerBuilder()
                .withActivity(this)
                .withHasStableIds(true)
                .withAccountHeader(headerResult)
                .withToolbar(toolbar)
                .withSliderBackgroundColorRes(R.color.colorPrimaryDark)
                .withOnDrawerItemClickListener { view, position, drawerItem ->
                    if (drawerItem != null) {
                        val intent: Intent? = null
                        when {
                            drawerItem.identifier == 1L -> {
                                val fragment = HomeFragment.newInstance()
                                replaceFragment(fragment)
                                toolbar.title = "Home Fragment"
                            }
                            drawerItem.identifier == 2L -> {
                                val fragment = MyProfileFragment.newInstance()
                                replaceFragment(fragment)
                                toolbar.title = "My Profile"
                            }
                        }

                        if (intent != null) {
                            this@MainActivity.startActivity(intent)
                        }
                    }
                    false
                }
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(false)


        drawerBuilder.addDrawerItems(PrimaryDrawerItem().withName("Home").withIcon(R.drawable.ic_home_white_24dp).withIdentifier(1).withTextColorRes(android.R.color.white).withSelectedColorRes(android.R.color.holo_green_dark).withSelectedTextColorRes(android.R.color.white))
        drawerBuilder.addDrawerItems(PrimaryDrawerItem().withName("My Profile").withIcon(R.drawable.ic_account_circle_white_24dp).withIdentifier(2).withTextColorRes(android.R.color.white).withSelectedColorRes(android.R.color.holo_green_dark).withSelectedTextColorRes(android.R.color.white))

        result = drawerBuilder.build()

        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        result?.actionBarDrawerToggle?.isDrawerIndicatorEnabled = true

        //only set the active selection or active profile if we do not recreate the activity
        if (savedInstanceState == null) {
            // set the selection to the data with the identifier 1
            result?.setSelection(1, false)
        }

    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
    }
}
